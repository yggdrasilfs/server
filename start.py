#!/usr/bin/python
# -*- coding: utf-8 -*-

from bottle import *
import os
import subprocess
from os import listdir
from os.path import isfile, join
import hashlib
from collections import OrderedDict

m = hashlib.md5()
BaseRequest.MEMFILE_MAX = 999999999

cwd = os.getcwd()
os.chdir("dir")

@route("/alive")
def alive():
    response.headers['Access-Control-Allow-Origin'] = '*'
    return "OK"
    
@route("/motd")
def motd():
    os.chdir(cwd)
    f = open("motd.txt", "r")
    x = f.read()
    f.close()
    os.chdir("dir")
    response.content_type = 'text/plain'
    response.headers['Access-Control-Allow-Origin'] = '*'
    return x

@route("/news")
def news():
    os.chdir(cwd)
    f = open("news.txt", "r")
    x = f.read()
    f.close()
    os.chdir("dir")
    response.content_type = 'text/plain'
    response.headers['Access-Control-Allow-Origin'] = '*'
    return x    

@route("/ls")
def ls():
    os.chdir(cwd)
    os.chdir("dir")
    files = [f for f in listdir(os.getcwd()) if isfile(join(os.getcwd(), f))]
    filesn = "\n".join(files)
    response.content_type = 'text/plain'
    filesn = filesn.replace(".password", "")
    filesn = "\n".join(list(OrderedDict.fromkeys(filesn.split("\n"))))
    return filesn
    
@route("/upload", method="POST")
def upload():
    os.chdir(cwd)
    os.chdir("dir")
    content = request.forms.get('content')
    filename = request.forms.get('filename')
    pwfilename = filename + ".password"
    password = request.forms.get('password')
    if ".." in filename:
        return "ERR_SECURITY"
    else:
        if isfile(pwfilename):
            f = open(filename + ".password")
            readpw = f.read()
            f.close()
        else:
            readpw = password
        if password == readpw:
            f = open(filename, "w")
            f.write(content)
            f.close()
            f = open(pwfilename, "w")
            f.write(password)
            f.close()
            return "OK"
        else:
            return "ERR_WRONG_PW"
    
@route("/download")
def download():
    filename = request.query['filename']
    os.chdir(cwd)
    os.chdir("dir")
    f = open(filename, "r")
    x = f.read()
    f.close()
    response.content_type = 'text/plain'
    return x
    
@route("/del")
def remove():
    os.chdir(cwd)
    os.chdir("dir")
    filename = request.query['filename']
    password = request.query['password']
    pwfilename = filename + ".password"
    if ".." in filename:
        return "ERR_SECURITY"
    else:
        f = open(pwfilename)
        s = f.read()
        f.close()
        if password == s:
            os.remove(filename)
            os.remove(pwfilename)
            return "OK"
        else:
            return "ERR_WRONG_PW"

@route("/files")
def getfiles():
    os.chdir(cwd)
    os.chdir("dir")
    files = [f for f in listdir(os.getcwd()) if isfile(join(os.getcwd(), f))]
    filesn = "\n".join(files)
    response.content_type = 'text/plain'
    for line in filesn:
        if not '.password' in line:
            fil += line
    return filesn
    
@route("/deactivated")
def deactivated():
    os.chdir(cwd)
    if isfile("deactivated.txt"):
        f = open("deactivated.txt", "r")
        x = f.read()
        f.close()
        return x
    else:
        return "ERR_ALLACTIVE"
        
@route("/stream")
def stream():
    os.chdir(cwd)
    if isfile("stream.txt"):
        f = open("stream.txt", "r")
        x = f.read()
        f.close()
        x = x.split('\n', 1)[0]
        response.headers['Access-Control-Allow-Origin'] = '*'
        return x
    else:
        return "ERR_NOSTREAM"
    
@route("/")
def index():
    return "This is a Yggdrasil server. Please use the <a href=\"http://yggdrasil.96.lt/\">client software</a> in order to interact with the server."

run(server="tornado", host="0.0.0.0", port=82)
