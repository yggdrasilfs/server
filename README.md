# Server

Yggdrasil server software licensed under GPLv3 (Bottle is licensed under MIT)

## Features

* Runs on any platform
* Written in Python
* Fully extensible
* You can even run your own website on top of the server software
* Runs with Bottle, that serves the API way faster than any other application like Apache

## Installation

### Windows

Download Python 2 or 3 and then download the ZIP file or clone this repository. After that just run the start.py and it should work.

### Linux

Just run these four commands for a successful server setup:

```bash
git clone https://github.com/yggdrasilfs/server.git
cd server
sudo pip install -r requirements.txt
sudo python start.py
```

If you get error messages, you can read the crash log. Mostly it's just a false configuration of the server (like the port is already used).

### macOS

Just download [Git](https://git-scm.com/) for Mac, install PIP and then run the same commands like on the Linux section

### It still doesn't work?

Then click the issue tab on this page and file a bug if you did everything right.

### Change the port

Just scroll to the very bottom of the software and there you'll see the port set to 82 in the run() function. Change this value to anything you want.
